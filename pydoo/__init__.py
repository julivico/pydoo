# -*- coding: utf-8 -*-

from .connector import Odoo
from .core.odoo_xmlrpc import OdooXmlRpc
from .models.model import Model
