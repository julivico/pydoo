# -*- coding: utf-8 -*-

from .field import Field


class Domain:
    def __init__(self, *args):
        self.criteria = []
        for arg in args:
            if isinstance(arg, Criterion):
                self.criteria.append(arg)

    def __str__(self):
        return str([str(criterion) for criterion in self.criteria])


class Criterion:
    def __init__(self, field_name, operator, value):
        self.field_name = field_name
        self.operator = operator
        self.value = value

    def __str__(self):
        return str([self.field_name, self.operator, self.value])


class Filter:
    def __init__(self, field, operator, value):
        """

        :param field: Use from
        :type field: Field
        :param operator:
        :param value:
        """

        # if not isinstance(field, Field):
        #    field = Field(field)
        self._field = field
        self._operator = operator
        self._value = value

    def __str__(self):
        return str((self._field, self._operator, self._value))
